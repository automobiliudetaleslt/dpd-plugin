<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\DpdPlugin\Factory\Model;

use Omni\Sylius\DpdPlugin\Model\ShipmentStatus;

class ShipmentStatusFactory
{
    /**
     * @param array $data
     * @return ShipmentStatus
     */
    public static function fromResponse(array $data): ShipmentStatus
    {
        return new ShipmentStatus($data['parcel_status'], $data['status'], $data['errlog']);
    }
}
