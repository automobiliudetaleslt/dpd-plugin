<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\DpdPlugin\Factory;

use BitBag\SyliusShippingExportPlugin\Entity\ShippingExport;
use BitBag\SyliusShippingExportPlugin\Entity\ShippingGatewayInterface;
use BitBag\SyliusShippingExportPlugin\Event\ExportShipmentEvent;
use Nfq\DpdClient\Constants\ServiceCodes;
use Nfq\DpdClient\Request\CreateShipmentRequest;
use Omni\Sylius\ShippingPlugin\Event\ExportShipmentsEvent;
use Sylius\Component\Core\Model\AddressInterface;
use Sylius\Component\Core\Model\OrderInterface;
use Sylius\Component\Core\Model\Shipment;
use Sylius\Component\Core\Model\ShipmentInterface;

class CreateShipmentRequestFactory
{
    public static function create(ExportShipmentEvent $exportShipmentEvent): CreateShipmentRequest
    {
        /** @var ShippingExport $shippingExport */
        $shippingExport = $exportShipmentEvent->getShippingExport();
        /** @var ShippingGatewayInterface $shippingGateway */
        $shippingGateway = $shippingExport->getShippingGateway();
        /** @var ShipmentInterface $shipment */
        $shipment = $shippingExport->getShipment();
        /** @var OrderInterface $order */
        $order = $shipment->getOrder();

        /** @var AddressInterface $shippingAddress */
        $shippingAddress = $order->getShippingAddress();
        $customer = $shippingAddress->getCustomer();

        $request = new CreateShipmentRequest();

        $request
            ->setName($shippingAddress->getFullName())
            ->setStreet($shippingAddress->getStreet())
            ->setCity($shippingAddress->getCity())
            ->setCountry($shippingAddress->getCountryCode())
            ->setPostCode($shippingAddress->getPostcode())
            ->setPhone($shippingAddress->getPhoneNumber())
            ->setEmail(null === $customer ? null : $customer->getEmail())
            ->setParcelNumber($order->getNumber())
            ->setNumberOfParcels(1)
            ->setParcelType($shippingGateway->getConfigValue('service_code'))
            ->setParcelShopId('LT90006') // @TODO
        ;

        if (ServiceCodes::STANDARD_PARCEL_PARCEL_SHOP === $shippingGateway->getConfigValue('service_code')) {
            // TODO: if payment method is cod
            $request->setCodAmount($shipment->getShippingUnitTotal() / 100);
        }

        return $request;
    }


    public static function createForMultipleShipments(ExportShipmentsEvent $exportShipmentsEvent): CreateShipmentRequest
    {
        /** @var ShippingExport[] $shippingExport */
        $shippingExports = $exportShipmentsEvent->getShippingExports();
        /** @var ShippingGatewayInterface $shippingGateway */
        $shippingGateway = $shippingExports[0]->getShippingGateway();

        /** @var Shipment $shipment */
        $shipment = $shippingExports[0]->getShipment();
        /** @var OrderInterface $order */
        $order = $shippingExports[0]->getShipment()->getOrder();

        /** @var AddressInterface $shippingAddress */
        $shippingAddress = $order->getShippingAddress();
        $customer = $shippingAddress->getCustomer();

        $request = new CreateShipmentRequest();

        $request
            ->setName($shippingAddress->getFullName())
            ->setStreet($shippingAddress->getStreet())
            ->setCity($shippingAddress->getCity())
            ->setCountry($shippingAddress->getCountryCode())
            ->setPostCode($shippingAddress->getPostcode())
            ->setPhone($shippingAddress->getPhoneNumber())
            ->setEmail(null === $customer ? null : $customer->getEmail())
            ->setParcelNumber(self::generateParcelNumber($order->getNumber(), $shipment->getId()))
            ->setDocumentReturnReferenceNumber($order->getNumber())
            ->setParcelType(ServiceCodes::STANDARD_PARCEL)
            ->setNumberOfParcels(count($shippingExports));

        if ($shipment->getParcelMachine() !== null) {
            $senderAddress = $order->getSenderAddress();
            $request
                ->setParcelShopId($shipment->getParcelMachine()->getCode())
                ->setPostCode($senderAddress->getPostCode())
                ->setParcelType(ServiceCodes::STANDARD_PARCEL_PARCEL_SHOP);
        }



        if ($order->getNotes()) {
            $request->setRemark(substr($order->getNotes(), 0, 45));
        }

        if (
            $shipment->getShipmentTotal() > 0 &&
            ServiceCodes::STANDARD_PARCEL_PARCEL_SHOP === $shippingGateway->getConfigValue('service_code')
        ) {
            $request->setCodAmount($shipment->getShipmentTotal()/100);
        }

        return $request;
    }


    /**
     * @param string $orderNumber
     * @param int $shipmentId
     * @return string
     */
    private static function generateParcelNumber(string $orderNumber, int $shipmentId): string
    {
        $strPadLimit = 14 - strlen($orderNumber);

        return sprintf('%s-%s-%s',
            $orderNumber,
            str_pad((string)$shipmentId, $strPadLimit, '0', STR_PAD_LEFT),
            substr((string) time(), -4)
        );
    }
}
