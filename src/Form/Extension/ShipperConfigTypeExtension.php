<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\DpdPlugin\Form\Extension;

use Omni\Sylius\ShippingPlugin\Form\AbstractShipperConfigType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;

class ShipperConfigTypeExtension extends AbstractTypeExtension
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add(
                'dpdHost',
                TextType::class,
                [
                    'label' => 'omni_sylius_dpd.form.credentials.dpd_host',
                    'required' => false,
                    'attr' => [
                        'class' => 'dpd-config hidden',
                    ],
                    'label_attr' => [
                        'class' => 'dpd-config hidden',
                    ],
                ]
            )
            ->add(
                'dpdUsername',
                TextType::class,
                [
                    'label' => 'omni_sylius_dpd.form.credentials.dpd_username',
                    'required' => false,
                    'attr' => [
                        'class' => 'dpd-config hidden',
                    ],
                    'label_attr' => [
                        'class' => 'dpd-config hidden',
                    ],
                ]
            )
            ->add(
                'dpdPassword',
                TextType::class,
                [
                    'label' => 'omni_sylius_dpd.form.credentials.dpd_password',
                    'required' => false,
                    'attr' => [
                        'class' => 'dpd-config hidden',
                    ],
                    'label_attr' => [
                        'class' => 'dpd-config hidden',
                    ],
                ]
            )
            ->add(
                'dpdSenderId',
                TextType::class,
                [
                    'label' => 'omni_sylius_dpd.form.credentials.dpd_sender_id',
                    'required' => false,
                    'attr' => [
                        'class' => 'dpd-config hidden',
                    ],
                    'label_attr' => [
                        'class' => 'dpd-config hidden',
                    ],
                ]
            )
            ->add(
                'dpdManifestTime',
                TimeType::class,
                [
                    'label' => 'omni_sylius_dpd.form.credentials.dpd_manifest_time',
                    'required' => false,
                    'input' => 'array',
                    'attr' => [
                        'class' => 'dpd-config hidden',
                    ],
                    'label_attr' => [
                        'class' => 'dpd-config hidden',
                    ],
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function getExtendedType(): string
    {
        return AbstractShipperConfigType::class;
    }
}
