<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\DpdPlugin\HttpClient;

use Http\Client\Common\Plugin\AuthenticationPlugin;
use Http\Client\Common\Plugin\BaseUriPlugin;
use Http\Client\Common\Plugin\ErrorPlugin;
use Http\Client\Common\Plugin\HeaderSetPlugin;
use Http\Client\Common\PluginClient;
use Http\Client\HttpClient;
use Omni\Sylius\DpdPlugin\Client\Client as DPDClient;
use Http\Discovery\HttpClientDiscovery;
use Http\Discovery\UriFactoryDiscovery;
use Http\Message\Authentication\QueryParam;
use Nfq\DpdClient\HttpClient\Client;
use Nfq\DpdClient\HttpClient\StatusFactory;

class DPDClientFactory
{
    /**
     * @var HttpClient
     */
    protected $client;

    /**
     * @var string
     */
    protected $username;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var string
     */
    protected $host = 'https://lt.integration.dpd.eo.pl';

    /**
     * DPDClientFactory constructor.
     * @param HttpClient $client
     * @param string $username
     * @param string $password
     * @param string $host
     */
    public function __construct(HttpClient $client, string $username, string $password, string $host)
    {
        $this->client = $client;
        $this->username = $username;
        $this->password = $password;
        $this->host = $host;
    }

    /**
     * @param string|null $host
     * @param string|null $username
     * @param string|null $password
     * @return DPDClient
     */
    public function create(?string $host = null, ?string $username = null, ?string $password = null): DPDClient
    {
        $username = $username ?? $this->username;
        $password = $password ?? $this->password;
        $host = $host ?? $this->host;

        if (null === $this->client) {
            $this->client = HttpClientDiscovery::find();
        }

        $queryParams = new QueryParam(['username' => $username, 'password' => $password]);

        $plugins = [
            new AuthenticationPlugin($queryParams),
            new BaseUriPlugin(UriFactoryDiscovery::find()->createUri($host), ['replace' => true]),
            new ErrorPlugin(),
            new HeaderSetPlugin(['Content-Type' => 'application/x-www-form-urlencoded']),
        ];

        $httpClient = new PluginClient($this->client, $plugins);
        return new DPDClient(
            new Client($httpClient),
            new Client(StatusFactory::create($httpClient))
        );
    }
}
