<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\DpdPlugin\Generator;

use BitBag\SyliusShippingExportPlugin\Entity\ShippingExport;
use Nfq\DpdClient\Client;
use Omni\Sylius\DpdPlugin\Constants\Gateway;
use Omni\Sylius\DpdPlugin\Factory\CloseManifestRequestFactory;
use Omni\Sylius\DpdPlugin\HttpClient\DPDClientFactory;
use Omni\Sylius\ManifestPlugin\Generator\ManifestGeneratorInterface;
use Omni\Sylius\ManifestPlugin\Generator\PathGenerator;
use Omni\Sylius\ManifestPlugin\Model\ManifestInterface;
use Omni\Sylius\ShippingPlugin\Provider\CredentialProviderInterface;
use Symfony\Component\Filesystem\Filesystem;

class ManifestGenerator implements ManifestGeneratorInterface
{
    /**
     * @var DPDClientFactory
     */
    private $factory;

    /**
     * @var PathGenerator
     */
    private $pathGenerator;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var CredentialProviderInterface
     */
    private $credentialProvider;

    /**
     * @param DPDClientFactory $factory
     * @param PathGenerator $pathGenerator
     * @param Filesystem $filesystem
     * @param CredentialProvider $credentialProvider
     */
    public function __construct(
        DPDClientFactory $factory,
        PathGenerator $pathGenerator,
        Filesystem $filesystem,
        CredentialProviderInterface $credentialProvider
    ) {
        $this->factory = $factory;
        $this->pathGenerator = $pathGenerator;
        $this->filesystem = $filesystem;
        $this->credentialProvider = $credentialProvider;
    }

    /**
     * {@inheritdoc}
     */
    public function generate(ManifestInterface $manifest): string
    {
        $client = $this->getClient($manifest);
        $response = $client->closeManifest(CloseManifestRequestFactory::create($manifest));

        if ('err' === $response->getStatus()) {
            throw new \Exception('Could not generate manifest for dpd.');
        }

        $path = $this->pathGenerator->generate(sprintf('%s-%s', $manifest->getSenderId(), $manifest->getShippingGateway()->getCode()));
        $this->filesystem->dumpFile($path, base64_decode($response->getPdf()));

        return basename($path);
    }

    public function getShipperName(): string
    {
        return Gateway::CODE;
    }

    /**
     * @param ManifestInterface $manifest
     * @return \Nfq\DpdClient\Client
     */
    private function getClient(ManifestInterface $manifest): Client
    {
        /** @var ShippingExport $shippingExport */
        $shippingExport = $manifest->getShippingExports()->first();
        $customer = $shippingExport->getShipment()->getOrder()->getCustomer();
        $credentials = $this->getCredentials($customer);

        if ($credentials !== null) {
            return $this->factory->create(
                $credentials['dpdHost'],
                $credentials['dpdUsername'],
                $credentials['dpdPassword']
            );
        }

        return $this->factory->create();
    }

    /**
     * @param $sender
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function getCredentials($sender): array
    {
        $credentials = $this->credentialProvider->getCredentials($sender->getId(), Gateway::CODE);

        if ($credentials === null) {
            throw new \Exception('No credentials found for this shipment.');
        }

        return $credentials['config'];
    }
}
